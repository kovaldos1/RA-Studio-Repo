let myMap = document.getElementById('map');

const spb = [
    { coordinates: [59.858426, 30.228498], balloonContentHeader: "<span>Проспект Маршала Жукова, 31, корп. 1,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.869643, 30.260406], balloonContentHeader: "<span>Проспект Стачек, 86</span>" },
    { coordinates: [59.844068, 30.178668], balloonContentHeader: "<span>Улица Партизана Германа, 2,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [60.014461, 30.388468], balloonContentHeader: "<span>Проспект Науки, 17,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.994698, 30.147495], balloonContentHeader: "<span>Лахтинский проспект, 85, лит. Б,<br>ТК &#034;Гарден Сити&#034;</span>" },
    { coordinates: [59.87696, 30.35932], balloonContentHeader: "<span>Улица Фучика, 2,<br>ТЦ &#034;Рио&#034;</span>" },
    { coordinates: [60.033994, 30.420585], balloonContentHeader: "<span>Гражданский проспект, 119,<br>ТЦ &#034;Рубикон&#034;</span>" },
    { coordinates: [60.044662, 30.388811], balloonContentHeader: "<span>Проспект Просвещения, 74, корп. 2, лит А,<br>Гипермаркет &#034;Перекрёсток&#034;</span>" },
    { coordinates: [60.039027, 30.409796], balloonContentHeader: "<span>Проспект Просвещения, 80,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [60.051953, 30.432972], balloonContentHeader: "<span>Бульвар Менделеева, 5, корпус 1</span>" },
    { coordinates: [60.056571, 30.437931], balloonContentHeader: "<span>Петровский б-р, д. 2 к. 1</span>" },
    { coordinates: [60.050273, 30.44519], balloonContentHeader: "<span>Привокзальная пл., 1А, корп.1</span>" },
    { coordinates: [60.050528, 30.422389], balloonContentHeader: "<span>Улица Шувалова, 1,<br>ЖК Гринландия</span>" },
    { coordinates: [59.897117, 30.422795], balloonContentHeader: "<span>Улица Бабушкина, 8, корпус 2,<br>ТЦ &#034;Елизаровский&#034;</span>" },
    { coordinates: [59.831942, 30.363209], balloonContentHeader: "<span>Дунайский проспект, 27, корп. 1, лит. Б,<br>ТК &#034;Дунай&#034;</span>" },
    { coordinates: [59.828844, 30.316111], balloonContentHeader: "<span>Пулковское шоссе, 17,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.831245, 30.344866], balloonContentHeader: "<span>Улица Звездная, 1,<br>ТРК &#034;Континент&#034;</span>" },
    { coordinates: [59.834406, 30.347103], balloonContentHeader: "<span>Улица Ленсовета, 80</span>" },
    { coordinates: [60.039589, 30.224141], balloonContentHeader: "<span>Комендантский пр, 69</span>" },
    { coordinates: [59.829899, 30.377835], balloonContentHeader: "<span>Балканская улица, 17,<br>ТРЦ &#034;Балканский-5&#034;</span>" },
    { coordinates: [59.73015, 30.615544], balloonContentHeader: "<span>Колпино, Оборонная улица, 2Б,<br>Гипермаркет &#034;Лента&#034; (ТК &#034;Ока&#034;)</span>" },
    { coordinates: [59.739069, 30.622795], balloonContentHeader: "<span>Колпино, Октябрьская улица, 8,<br>ТК &#034;Ока&#034;</span>" },
    { coordinates: [59.737369, 30.572095], balloonContentHeader: "<span>Колпино, бульвар Трудящихся, 12,<br>ТК &#034;Ока&#034;</span>" },
    { coordinates: [59.740588, 30.58406], balloonContentHeader: "<span>Колпино, улица Пролетарская, 36 лит. А 1 этаж,<br>ТК &#034;Меркурий&#034;</span>" },
    { coordinates: [59.806821, 30.369282], balloonContentHeader: "<span>Новгородский проспект, 10</span>" },
    { coordinates: [59.827514, 30.399124], balloonContentHeader: "<span>Улица Олеко Дундича, 17, корп. 1,<br>Совместный вход с детской стоматологией</span>" },
    { coordinates: [59.930593, 30.432056], balloonContentHeader: "<span>Заневский проспект, 65, корп. 1,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.933302, 30.440222], balloonContentHeader: "<span>Заневский проспект, 71,<br>ТЦ Заневский Каскад-2 прикассовая зона м-на &#034;Лента&#034;</span>" },
    { coordinates: [59.945986, 30.472265], balloonContentHeader: "<span>Индустриальный проспект, 24, лит. А,<br>ТРК &#034;Июнь&#034;</span>" },
    { coordinates: [59.950244, 30.488237], balloonContentHeader: "<span>Проспект Наставников, 35, корп. 1</span>" },
    { coordinates: [59.859406, 30.390581], balloonContentHeader: "<span>Бухарестская улица, 47</span>" },
    { coordinates: [59.870822, 30.377412], balloonContentHeader: "<span>Улица Белы Куна, 6, корп. 1</span>" },
    { coordinates: [59.870091, 30.379388], balloonContentHeader: "<span>Улица Белы Куна, 3, лит. А,<br>ТЦ &#034;Международный&#034;</span>" },
    { coordinates: [59.851214, 30.350139], balloonContentHeader: "<span>Проспект Космонавтов, 45, лит. А,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.927545, 30.413668], balloonContentHeader: "<span>Новочеркасский проспект, 32, корп. 1</span>" },
    { coordinates: [59.940274, 30.417994], balloonContentHeader: "<span>Улица Якорная, 5А,<br>ТРЦ &#034;Охта Молл&#034;</span>" },
    { coordinates: [60.038294, 30.320728], balloonContentHeader: "<span>Выборгское шоссе, 3, корп. 1, лит. А,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.859926, 30.306247], balloonContentHeader: "<span>Новоизмайловский проспект, 28, корп. 1</span>" },
    { coordinates: [59.868948, 30.35048], balloonContentHeader: "<span>Проспект Космонавтов, 14, лит. А,<br>ТРЦ &#034;Питер-Радуга&#034;</span>" },
    {
        coordinates: [60.091532, 30.379949],
        balloonContentHeader: "<span>Пересечение КАД и автодороги СПб-Скотное, деревня Порошкино, 117,<br>ТРЦ &#034;Мега-Парнас&#034;, 2 этаж, прикассовая зона гипермаркета Ашан</span>",
    },
    { coordinates: [60.066958, 30.336287], balloonContentHeader: "<span>Улица Михаила Дудина, 6, корп. 1,<br>ТЦ &#034;Парнас Сити&#034;</span>" },
    { coordinates: [60.069446, 30.332403], balloonContentHeader: "<span>Улица Федора Абрамова, 4, корп. 1, лит. А,<br>Вход со стороны улицы Михаила Дудина</span>" },
    { coordinates: [60.000285, 30.271949], balloonContentHeader: "<span>Богатырский проспект, 13,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.993001, 30.353265], balloonContentHeader: "<span>Улица Карбышева, 9,<br>Гипермаркет &#034;Карусель&#034;</span>" },
    { coordinates: [59.949907, 30.231903], balloonContentHeader: "<span>Наличная улица, 40, корп. 1</span>" },
    { coordinates: [59.849202, 30.144245], balloonContentHeader: "<span>Петергофское шоссе, 51,<br>Торговый Центр &#034;Жемчужная Плаза&#034;</span>" },
    { coordinates: [59.796026, 30.146976], balloonContentHeader: "<span>Таллинское шоссе, 27,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.853473, 30.146383], balloonContentHeader: "<span>Улица Адмирала Трибуца, 5, лит. А</span>" },
    { coordinates: [59.839832, 30.104521], balloonContentHeader: "<span>Улица Генерала Кравченко, 8, стр. 1,<br>ЖК Солнечный город</span>" },
    { coordinates: [59.8249, 30.173413], balloonContentHeader: "<span>Улица Партизана Германа, 47,<br>Гипермаркет &#034;Карусель&#034;</span>" },
    { coordinates: [60.052153, 30.332814], balloonContentHeader: "<span>Прoспект Просвещения, 19, лит. А,<br>Торговый комплекс &#034;2000 Норд&#034;</span>" },
    { coordinates: [60.048283, 30.3526], balloonContentHeader: "<span>Проспект Художников, 26</span>" },
    { coordinates: [59.831417, 30.502384], balloonContentHeader: "<span>Тепловозная улица, 31,<br>ТРЦ Порт Находка</span>" },
    { coordinates: [60.002899, 30.228471], balloonContentHeader: "<span>Богатырский проспект, 42,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.990305, 30.260721], balloonContentHeader: "<span>Торфяная дорога, 7, лит. Б,<br>ТРК &#034;Гулливер&#034;</span>" },
    { coordinates: [59.988831, 30.225412], balloonContentHeader: "<span>Улица Савушкина, 119, корп. 3,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.901373, 30.51162], balloonContentHeader: "<span>Европейский проспект, 9, корп. 1</span>" },
    { coordinates: [59.893952, 30.514275], balloonContentHeader: "<span>Мурманское шоссе, 12 км., ,<br>ТРЦ &#034;Мега-Дыбенко&#034;</span>" },
    { coordinates: [59.912003, 30.480125], balloonContentHeader: "<span>Проспект Большевиков, 10,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.986214, 30.296321], balloonContentHeader: "<span>Улица Савушкина, 9</span>" },
    { coordinates: [59.959602, 30.289392], balloonContentHeader: "<span>Улица Большая Разночинная, 16,<br>ТЦ &#034;Чкаловский&#034;</span>" },
    { coordinates: [59.88143, 30.315473], balloonContentHeader: "<span>Московский проспект, 137,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.791576, 30.165805], balloonContentHeader: "<span>Бульвар Десантника Вадима Чугунова, 1, к 1,<br>п. Новогорелово</span>" },
    { coordinates: [60.013096, 30.251549], balloonContentHeader: "<span>Комендантский проспект, 24 к1</span>" },
    { coordinates: [60.030829, 30.243949], balloonContentHeader: "<span>Проспект Королёва, 61, Лит.А, пом.46Н,<br>ЖК Юбилейный квартал</span>" },
    { coordinates: [59.926499, 30.320845], balloonContentHeader: "<span>Улица Ефимова, 2,<br>Цокольный этаж ТРК &#034;ПИК&#034;, прикассовая зона ГМ &#034;Перекресток&#034;</span>" },
    { coordinates: [60.023155, 30.220602], balloonContentHeader: "<span>Улица Планерная, 63</span>" },
    { coordinates: [60.032955, 30.629165], balloonContentHeader: "<span>г. Всеволожск, Дорога Жизни, 11,<br>Cупермаркет &#034;Пятерочка&#034;</span>" },
    { coordinates: [60.029033, 30.639061], balloonContentHeader: "<span>г. Всеволожск, улица Ленинградская, 38,<br>Cупермаркет &#034;Пятерочка&#034;</span>" },
    { coordinates: [59.583589, 30.13863], balloonContentHeader: "<span>г. Гатчина, Ленинградское шоссе, 12,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [59.742919, 30.47284], balloonContentHeader: "<span>п. Шушары, терр. Славянка, ул. Ростовская, д. 20, стр.1, часть № 59, пом.5-Н,<br>рядом с Перекрёстком</span>" },
];

const msk = [
    { coordinates: [55.888296, 37.588674], balloonContentHeader: "<span>Алтуфьевское шоссе, 70к1,<br>ТЦ Маркос Молл, Cats&amp;Dogs</span>" },
    { coordinates: [55.864778, 37.466323], balloonContentHeader: "<span>Ленинградское шоссе, 112, корус 3</span>" },
    { coordinates: [55.898662, 37.629379], balloonContentHeader: "<span>МКАД 87 км, 8 - Корнейчука, 8, ,<br>ТК &#034;Час Пик&#034;</span>" },
    { coordinates: [55.861802, 37.562955], balloonContentHeader: "<span>Бескудниковский бульвар, 12,<br>Cats&amp;Dogs</span>" },
    { coordinates: [55.612045, 37.732718], balloonContentHeader: "<span>Ореховый бульвар, 22А,<br>ТЦ Облака, Cats&amp;Dogs</span>" },
    { coordinates: [55.744413, 37.566268], balloonContentHeader: "<span>Площадь Киевского вокзала, 2,<br>ТЦ &#034;Европейский&#034;</span>" },
    { coordinates: [55.766212, 37.380875], balloonContentHeader: "<span>Рублевское шоссе, 62,<br>ТЦ Европарк Cats&amp;Dogs</span>" },
    { coordinates: [55.690278, 37.601879], balloonContentHeader: "<span>Большая Черёмушкинская улица, 1,<br>ТЦ &#034;РИО&#034;, Cats&amp;Dogs</span>" },
    { coordinates: [55.846113, 37.358211], balloonContentHeader: "<span>Дубравная улица, 34/29,<br>ТЦ Ладья, Cats&amp;Dogs</span>" },
    { coordinates: [55.799588, 37.483023], balloonContentHeader: "<span>Улица Маршала Бирюзова, 32,<br>ТЦ Пятая Авеню, Cats&amp;Dogs</span>" },
    { coordinates: [55.793551, 37.803991], balloonContentHeader: "<span>Улица Первомайская, 87,<br>Cats&amp;Dogs</span>" },
    { coordinates: [55.864454, 37.397413], balloonContentHeader: "<span>д. Путилково, МКАД 71 км, 1,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [55.611114, 37.606622], balloonContentHeader: "<span>Улица Красного Маяка, 2Б,<br>ТРЦ Columbus, Cats&amp;Dogs</span>" },
    { coordinates: [55.668635, 37.518758], balloonContentHeader: "<span>Ленинский проспект, 99,<br>Cats&amp;Dogs</span>" },
    { coordinates: [55.855078, 37.478082], balloonContentHeader: "<span>Фестивальная улица, 2Б,<br>ТЦ Речной, Cats&amp;Dogs</span>" },
    { coordinates: [55.647041, 37.411741], balloonContentHeader: "<span>Солнцевский проспект, 21,<br>ТЦ &#034;Столица&#034;, Cats&amp;Dogs</span>" },
    { coordinates: [55.877354, 37.558957], balloonContentHeader: "<span>Дубнинская улица, 30,<br>ТЦ Петровский, Cats&amp;Dogs</span>" },
    { coordinates: [55.850408, 37.44409], balloonContentHeader: "<span>Сходненская улица, 56,<br>Калейдоскоп Cats&amp;Dogs</span>" },
    { coordinates: [55.619467, 37.50928], balloonContentHeader: "<span>Новоясеневский проспект, 1,<br>ТЦ Спектр, Cats&amp;Dogs</span>" },
    { coordinates: [55.658794, 37.401905], balloonContentHeader: "<span>Боровское шоссе, 6,<br>ТЦ Солнечный, Cats&amp;Dogs</span>" },
    { coordinates: [55.591533, 37.455714], balloonContentHeader: "<span>Бульвар Веласкеса, 2,<br>Поселение Сосенское</span>" },
    { coordinates: [55.799374, 37.273415], balloonContentHeader: "<span>Новорижское шоссе 23 км, вл.2 , ст.1,<br>ТРК РИГА-МОЛЛ</span>" },
    { coordinates: [55.800071, 37.280367], balloonContentHeader: "<span>Новорижское шоссе 23 км, ст1, ,<br>ТЦ Юнимолл (рядом с д. Воронки)</span>" },
    { coordinates: [55.78236, 37.236085], balloonContentHeader: "<span>Новорижское шоссе 26 км, 1, ,<br>Гипермаркет &#034;Карусель&#034;</span>" },
    { coordinates: [55.831719, 38.393709], balloonContentHeader: "<span>Ногинск, Горьковское шоссе 50км, 5,<br>Гипермаркет &#034;О&#039;Кей&#034;</span>" },
    { coordinates: [55.850995, 37.597414], balloonContentHeader: "<span>Сигнальный проезд, 17,<br>ГМ Ашан, Cats&amp;Dogs</span>" },
    { coordinates: [55.643053, 37.391107], balloonContentHeader: "<span>Улица Производственная, 12, стр. 2</span>" },
    { coordinates: [55.643053, 37.391107], balloonContentHeader: "<span>Улица Производственная, 12, стр. 2</span>" },
    { coordinates: [55.705417, 36.191551], balloonContentHeader: "<span>Улица Федеративная, 7,<br>ТЦ &#034;Руза Хутор&#034;</span>" },
];

if (myMap) {
    ymaps.ready(init);
    function init(){
        myMap = new ymaps.Map ("map", {
            center: [59.939979, 30.316859],
            behaviors: ['default', 'scrollZoom'],
            zoom: 10,
            controls: ['smallMapDefaultSet']
        });

        for (const point of spb) {
            myPlacemark = new ymaps.Placemark(point.coordinates, {
                balloonContentHeader: point.balloonContentHeader,
            });
            myMap.geoObjects.add(myPlacemark);
        }

        for (const point of msk) {
            myPlacemark = new ymaps.Placemark(point.coordinates, {
                balloonContentHeader: point.balloonContentHeader,
            });
            myMap.geoObjects.add(myPlacemark);
        }

    }
}

let toSPetersburg = document.getElementById("to-S-Petersburg");
let toMoscow = document.getElementById("to-Moscow");

toSPetersburg.addEventListener('click', function(event) {
    event.preventDefault();
    myMap.setCenter([59.96, 30.35], 10);
});

toMoscow.addEventListener('click', function(event) {
    event.preventDefault();
    myMap.setCenter([55.74, 37.56], 10);
});

//плавный переход к якорям
const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
    anchor.addEventListener("click", (event) => {
        event.preventDefault();
        const blockId = anchor.getAttribute('href')
        document.querySelector('' + blockId).scrollIntoView({
            behavior: "smooth",
            block: "start"
        })
    })
}